package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Wallet
 *
 */
@Entity

public class Wallet implements Serializable {

	
	private Integer id;
	private Float walletValue;
	private Integer valid;
	private static final long serialVersionUID = 1L;

	public Wallet() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Float getWalletValue() {
		return this.walletValue;
	}

	public void setWalletValue(Float walletValue) {
		this.walletValue = walletValue;
	}   
	public Integer getValid() {
		return this.valid;
	}

	public void setValid(Integer valid) {
		this.valid = valid;
	}
   
}
