package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Securities
 *
 */
@Entity

public class Securities implements Serializable {

	
	private Integer id;
	private String value;
	private Date session;
	private Float closingPrice;
	private Float variabiliy;
	private Integer quantity;
	private static final long serialVersionUID = 1L;

	public Securities() {
		super();
	}   
	@Id    
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}   
	public Date getSession() {
		return this.session;
	}

	public void setSession(Date session) {
		this.session = session;
	}   
	public Float getClosingPrice() {
		return this.closingPrice;
	}

	public void setClosingPrice(Float closingPrice) {
		this.closingPrice = closingPrice;
	}   
	public Float getVariabiliy() {
		return this.variabiliy;
	}

	public void setVariabiliy(Float variabiliy) {
		this.variabiliy = variabiliy;
	}   
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
   
}
